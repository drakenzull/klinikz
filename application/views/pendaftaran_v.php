<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AdminLTE 2 | Starter</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="<?php echo base_url("assets/bootstrap/css/bootstrap.min.css") ?>">
        <!-- jQueryUI -->
        <link rel="stylesheet" href="<?php echo base_url("assets/plugins/jQueryUI/jquery-ui.min.css") ?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url("assets/plugins/fontawesome/css/font-awesome.min.css") ?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url("assets/plugins/select2/select2.min.css") ?>">

        <!-- bootstrap toggle -->
        <link rel="stylesheet" href="<?php echo base_url("assets/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css") ?>">
        <!-- Ionicons -->
        <!--link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"--!>
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url("assets/dist/css/AdminLTE.min.css") ?>">
        <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
              page. However, you can choose any other skin. Make sure you
              apply the skin class to the body tag so the changes take effect.
        -->
        <link rel="stylesheet" href="<?php echo base_url("assets/dist/css/skins/skin-blue.min.css") ?>">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
    -->
    <body class="hold-transition skin-blue layout-top-nav">
        <div class="wrapper">
            <?php $this->load->view('template/header') ?>

            <div class="content-wrapper">


                <!-- Content Wrapper. Contains page content -->
                <section class="content">

                    <form role="form" action="models/reg.php" method="POST" class="form-horizontal">

                        <div class="box box-primary box-solid">

                            <div class="box-header with-border">
                                <h5 class="box-title">
                                    <i class="fa fa-fw fa-table"></i>
                                    Pendaftaran Data Pasien
                                </h5>

                            </div>

                            <div class="box-body">


                                <div class="row">
                                    <div class="col-xs-12">

                                        <input type="hidden" id="kondisi_load" value="0"> <!-- fitur ini untuk menyiasati event change select2 di element wilayah -->

                                        <!-- PAGE CONTENT BEGINS -->

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <label>Status Pasien</label>


                                                        <input id="pasien" checked data-toggle="toggle" data-width="200" data-onstyle="success" data-offstyle="danger" data-on="<i class='fa fa-user'></i> PASIEN BARU" data-off="<i class='fa fa-user'></i> PASIEN LAMA" type="checkbox">
                                                        <input type="hidden" id="PASIEN_BARU" name="PASIEN_BARU" value="1">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                        <h3 class="header blue lighter smaller">Data Pasien</h3>

                                        <div class="row"> <!-- first row -->
                                            <div class="col-sm-3">
                                                <label class="control-label" for="NOMR">NO.MR</label>
                                                <div class="input-group">

                                                    <input type="text" class="form-control" id="NORM" NAME="NORM" placeholder="No. Rekam Medis" readonly="readonly" data-validation="required">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-sm btn-danger" disabled="disabled"  id="TOMBOLCARI" type="button">
                                                            <i class="ace-icon fa fa-search bigger-110"></i>

                                                        </button>
                                                    </span>
                                                </div>

                                            </div>
                                            <div class="col-sm-4">
                                                <label class="control-label" for="NAMA">NAMA PASIEN</label>
                                                <input type="text" class="form-control" id="NAMA" NAME="NAMA" placeholder="Masukkan Nama Pasien" data-validation="required" data-validation-error-msg="Nama Pasien Harus di isi">
                                            </div>
                                            <div class="col-md-4">

                                                <label class="control-label" for="KELUARGA">NAMA KELUARGA</label>
                                                <input type="text" class="form-control" id="KELUARGA" NAME="KELUARGA" placeholder="NAMA KELUARGA PASIEN" data-validation="required" data-validation-error-msg="Nama Keluarga Pasien">
                                            </div>
                                        </div>
                                        <div class="row"> <!-- first row -->




                                            <div class="col-md-2">

                                                <label class="control-label" for="JK">JENIS KELAMIN</label>
                                                <select id="JK" NAME="JK" class="form-control" data-validation="required" data-validation-error-msg="Jenis Kelamin Pasien Harus di Pilih">
                                                    <option value="">-- Pilih Jenis Kelamin --</option>
                                                    <option value="l">LAKI - LAKI</option>
                                                    <option value="p">WANITA</option>
                                                </select>

                                            </div>
                                            <div class="col-md-4">

                                                <label class="control-label" for="NOMR">TEMPAT LAHIR</label>
                                                <input type="text" class="form-control" id="TEMPAT_LAHIR" NAME="TEMPAT_LAHIR" placeholder="TEMPAT LAHIR PASIEN" data-validation="required" data-validation-error-msg="Tempat Lahir Pasien Harus diisi">

                                            </div>
                                            <div class="col-md-2">

                                                <label class="control-label" for="NAMA">TANGGAL LAHIR</label>

                                                <input class="form-control" id="TGLLAHIR" NAME="TGLLAHIR" value="<?php echo date("d-m-Y"); ?>" type="text" data-validation="required" data-validation-error-msg="Tanggal Lahir Pasien Harus di isi">



                                            </div>






                                        </div>

                                        <h3 class="header blue lighter smaller">Data Sosial Pasien</h3>


                                        <div class="row"> <!-- second row -->



                                            <div class="col-md-3">

                                                <label class="control-label" for="MARITAL">STATUS MARITAL</label>
                                                <select id="MARITAL" NAME="MARITAL" class="form-control" data-validation="required" data-validation-error-msg="Status Pernikahan Harus di isi">
                                                    <option value="">-- Pilh Status--</option>
                                                    <option value="1">BELUM MENIKAH</option>
                                                    <option value="2">MENIKAH</option>
                                                    <option value="3">JANDA</option>
                                                    <option value="4">DUDA</option>
                                                </select>

                                            </div>

                                            <div class="col-md-3">

                                                <label class="control-label" for="AGAMA">AGAMA</label>
                                                <select id="AGAMA" NAME="AGAMA" class="form-control" data-validation="required" data-validation-error-msg="Agama Harus di isi">
                                                    <option value="">-- PILIH AGAMA --</option>
                                                    <option value="1">ISLAM</option>
                                                    <option value="2">KRISTEN</option>
                                                    <option value="3">HINDU</option>
                                                    <option value="3">BUDHA</option>
                                                    <option value="3">KONG HU CHU</option>
                                                </select>

                                            </div>

                                            <div class="col-md-3">

                                                <label class="control-label" for="PENDIDIKAN">PENDIDIKAN</label>
                                                <select id="PENDIDIKAN" NAME="PENDIDIKAN" class="form-control" data-validation="required" data-validation-error-msg="Pendidikan Terakhir Harus di pilih">


                                                    <option value="">-- Pilih Pendidikan -- </option>
                                                    <?php
                                                    foreach ($pendidikan as $row) {
                                                        echo '<option value="' . $row->kd_pendidikan . '">' . $row->nama_pendidikan . '</option>';
                                                    }
                                                    ?>
                                                </select>

                                            </div>
                                            <div class="col-md-3">

                                                <label class="control-label" for="PEKERJAAN">PEKERJAAN</label>
                                                <select id="PEKERJAAN" NAME="PEKERJAAN" class="form-control" data-validation="required" data-validation-error-msg="Pekerjaan Pasien Harus dipilih">
                                                    <option value="">-- Pilih Pekerjaan--</option>
                                                    <?php
                                                    foreach ($pekerjaan as $row) {
                                                        echo '<option value="' . $row->kd_pekerjaan . '">' . $row->nama_pekerjaan . '</option>';
                                                    }
                                                    ?>
                                                </select>

                                            </div>

                                        </div> <!-- end second row -->

                                        <div class="row"> <!-- third row -->

                                            <div class="col-md-4">

                                                <label class="control-label" for="KEC">NO.TELP/HANDPHONE</label>
                                                <input type="text" class="form-control" id="TELP" NAME="TELP" placeholder="No. TELP" data-validation="required" data-validation-error-msg="No Handphone Harus di isi">

                                            </div>



                                            <div class="col-md-4">

                                                <label class="control-label" for="KTP">NO KTP</label>
                                                <input type="text" class="form-control" id="KTP" name="KTP">



                                            </div>

                                            <div class="col-md-4">

                                                <label class="control-label" for="KEC">NO ASURANSI</label>
                                                <input type="text" class="form-control" id="ASURANSI" name="ASURANSI">

                                                </select>

                                            </div>




                                        </div> <!-- end third row -->

                                        <h3 class="header blue lighter smaller">Data Wilayah Pasien</h3>

                                        <div class="row"> <!-- third row -->
                                            <div class="col-md-4">

                                                <label class="control-label" for="ALAMAT">ALAMAT SEKARANG</label>
                                                <input type="text" class="form-control" id="ALAMAT" NAME="ALAMAT" placeholder="ALAMAT" data-validation="required" data-validation-error-msg="Harus di isi">

                                            </div>
                                        </div>
                                        <div class="row"> <!-- third row -->

                                            <div class="col-md-3">

                                                <label class="control-label" for="PROVINSI">PROVINSI</label>
                                                <select id="PROVINSI" NAME="PROVINSI" class="form-control PROV" data-validation="required" data-validation-error-msg="Provinsi Harus di pilih">
                                                    <option value="">-- Pilih Provinsi --</option>
                                                    <?php
                                                    foreach ($provinsi as $row) {
                                                        echo "<option value=" . $row->KODE_PROV . ">" . $row->NAMA_PROV . "</option>";
                                                    }
                                                    ?>

                                                </select>

                                            </div>

                                            <div class="col-md-3">

                                                <label class="control-label" for="KAB">KABUPATEN/KOTA</label>
                                                <select id="KAB" NAME="KAB" class="form-control KAB" data-validation="required" data-validation-error-msg="Kabupaten Harus di pilih">
                                                    <option value="">-- Pilih Kabupaten--</option>
                                                </select>

                                            </div>

                                            <div class="col-md-3">

                                                <label class="control-label" for="KEC">KECAMATAN</label>
                                                <select id="KEC" NAME="KEC" class="form-control KEC" data-validation="required" data-validation-error-msg="Kecamatan Harus di isi">
                                                    <option value="">-- Pilih Kecamatan--</option>
                                                </select>

                                            </div>


                                            <div class="col-md-3">

                                                <label class="control-label" for="NAMA">DESA/KELURAHAN</label>
                                                <select id="KEL" NAME="KEL" class="form-control KEL" data-validation="required" data-validation-error-msg="Desa/Kel Harus di isi">
                                                    <option value="">-- Pilih Kel--</option>
                                                </select>

                                            </div>


                                        </div> <!-- end third row -->
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="box box-solid box-primary" id="widget-box-2">
                            <div class="box-header with-header">
                                <h5 class="box-title">
                                    <i class="ace-icon fa fa-table"></i>
                                    Kunjungan Pasien
                                </h5>

                            </div>

                            <div class="box-body">


                                <div class="row">
                                    <div class="col-md-2">
                                        <label class="control-label" for="bayar">Tanggal Kunjungan</label>
                                        <input type="text" data-validation="required" name="TGLKUNJUNGAN" id="TGLKUNJUNGAN" class="form-control" value="<?php echo date("d-m-Y"); ?>" size="10" data-validation="required" data-validation-error-msg="Tanggal Kunjungan Harus di isi">
                                    </div>
                            
                                    <div class="col-md-3">
                                        <label class="control-label" for="bayar">Tipe Pasien</label>
                                        <select id="bayar" NAME="BAYAR" class="form-control" data-validation="required" data-validation-error-msg="Pilih Tipe Pasien">
                                            <option value="">-- Pilih Carabayar--</option>
                                            <?php
                                            foreach ($kelompok as $row) {
                                                echo "<option value=" . $row->kd_bayar . ">" . $row->nama_kelompok . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>


                                    <div class="col-md-3">

                                        <label class="control-label" for="NAMA">POLI</label>
                                        <select id="POLI" NAME="POLI" class="form-control" data-validation="required" data-validation-error-msg="Poli Tujuan Harus di isi">
                                            <option value="">-- Pilih Poli--</option>
                                            <?php
                                            foreach ($unit as $row) {
                                                echo "<option value=" . $row->kd_unit . ">" . $row->nama_unit . "</option>";
                                            }
                                            ?>

                                        </select>


                                    </div>
                                    <div class="col-md-4">

                                        <label class="control-label" for="NAMA">DOKTER</label>
                                        <select id="DOKTER" NAME="DOKTER" class="form-control" data-validation="required" data-validation-error-msg="Dokter Harus di Isi">
                                            <option value="">-- Pilih Dokter--</option>
                                            <?php
                                            foreach ($dokter as $row) {
                                                echo "<option value=" . $row->kd_dokter . ">" . $row->nama_dokter . "</option>";
                                            }
                                            ?>

                                        </select>



                                    </div>
                                </div>
                            </div>


                                <div class="box-footer">
                                    <button type="submit" class="btn btn-lg btn-success pull-right">
                                        <i class="fa fa-save"></i>
                                        SIMPAN
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>






                    <!-- form ambil data pasien -->
                    <div id="dialog-form" title="Create new user">


                        <fieldset>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="name" class="control-label col-sm-3">Name Pasien</label>
                                    <div class="col-sm-6">
                                        <input type="text" id="cari_nama" class="text ui-widget-content ui-corner-all form-control">
                                    </div>
                                    <button id="searchpasien" class="btn btn-cari btn-sm btn-primary">Cari <i class="fa fa-search"></i></button>
                                </div>


                                <table class="table table-condesed table-stripped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>No.MR</th>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>Keluarga</th>
                                            <th>Pilih</th>
                                        </tr>
                                    </thead>
                                    <tbody id="list_pasien">

                                    </tbody>
                                </table>

                        </fieldset>

                    </div>

                </section>





            </div><!-- /.content-wrapper -->
            <?php $this->load->view('template/footer') ?>
            <?php $this->load->view('template/control') ?>
        </div> <!-- /.wrapper -->

        <!-- jQuery 2.1.4 -->
        <script src="<?php echo base_url("assets/plugins/jQuery/jQuery-2.1.4.min.js"); ?>"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="<?php echo base_url("assets/bootstrap/js/bootstrap.min.js"); ?>"></script>
        <!-- SlimScroll -->
        <script src="<?php echo base_url("assets/plugins/slimScroll/jquery.slimscroll.min.js"); ?>"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url("assets/plugins/fastclick/fastclick.min.js"); ?>"></script>
        <!-- select2 -->
        <script src="<?php echo base_url("assets/plugins/select2/select2.min.js"); ?>"></script>
        <!-- form-validator -->
        <script src="<?php echo base_url("assets/plugins/form-validator/jquery.form-validator.min.js"); ?>"></script>
        <!-- jqueryUI -->
        <script src="<?php echo base_url("assets/plugins/jQueryUI/jquery-ui.min.js"); ?>"></script>
        <!-- maskinput -->
        <script src="<?php echo base_url("assets/plugins/maskinput/jquery.mask.min.js"); ?>"></script>
        <!-- hotkeys -->
        <script src="<?php echo base_url("assets/plugins/hotkeys/jquery.hotkeys.js"); ?>"></script>

        <!-- iCheck -->
        <script src="<?php echo base_url("assets/plugins/bootstrap-toggle/js/bootstrap-toggle.min.js"); ?>"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url("assets/dist/js/app.min.js"); ?>"></script>
        <script type="text/javascript">

            $(document).ready(function () {


                //fungsi event tombol jenis pasien


                $('#pasien').change(function () {
                    var status = $(this).prop('checked');

                    if (status) {

                        $("#NORM").attr("readonly", "readonly");
                        $("#TOMBOLCARI").attr("disabled", "disabled");
                        $("#NORM").unmask();
                        $("#NORM").val("-- Otomatis--");
                        $("#NORM").attr("disabled", "disabled");
                        console.log("nilainya : " + status);
                        $("#PASIEN_BARU").val("1");
                    } else {

                        $("#NORM").removeAttr("readonly");
                        $("#NORM").val("");
                        $("#TOMBOLCARI").removeAttr("disabled");
                        $("#NORM").unmask().mask("00-00-00");
                        console.log("nilainya :" + status);
                        $("#PASIEN_BARU").val("0");
                    }
                });

                //form validator
                $.validate();



                $("#DAFTAR_LAMA").click(function () {
                    $("#NORM").removeAttr("disabled");
                    $("#TOMBOLCARI").removeAttr("disabled");
                });



                $("#TELP").mask("0000-0000-0000");
                $("#NORM").mask("00-00-00");

                $("#DAFTAR_BARU").click(function () {

                    $("#NORM").attr("disabled", "disabled");
                    $("#TOMBOLCARI").attr("disabled", "disabled");

                });

                $('#NORM').blur(function () {
                    var nomr = $(this).val();

                    if (nomr != "" && nomr.length == 8) {
                        format_mr = nomr.replace(/-/g, "");
                        load_pasien(format_mr);
                    }
                }); //end of $("#NORM").blur();

                $("#list_pasien").on("click", ".btn-pilih", function () {
                    nomr = $(this).attr("data-mr");
                    load_pasien(nomr);
                    $("#list_pasien").empty();
                    dialog.dialog("close");
                });





                //start function load_pasien

                function load_pasien(mr) {

                    $.post("<?php echo base_url("pasien/load"); ?>", {nomr: mr}, function (feed) {

                        if (feed.status) {

                            $("#NORM").val(feed.data[0].MR);
                            $("#PROV, #KAB, #KEC, #KEL").select2("destroy");


                            $("#NAMA").val(feed.data[0].NAMA);
                            $("#KELUARGA").val(feed.data[0].KELUARGA);
                            $("#TEMPAT_LAHIR").val(feed.data[0].TEMPAT_LAHIR);
                            $("#KTP").val(feed.data[0].KTP);
                            $("#ASURANSI").val(feed.data[0].NOKARTU);
                            $("#TELP").val(feed.data[0].TELPON);
                            $("#ALAMAT").val(feed.data[0].ALAMAT);
                            var year = feed.data[0].TGL_LAHIR.substr(0, 4);
                            var month = feed.data[0].TGL_LAHIR.substr(5, 2);
                            var day = feed.data[0].TGL_LAHIR.substr(8, 2);
                            $("#TGLLAHIR").val(feed.data[0].TGLLAHIR);

                            $('#JK').val(feed.data[0].GENDER);
                            $('#JK').trigger("change");
                            $('#MARITAL').val(feed.data[0].MARITAL).trigger("change");
                            $('#AGAMA').val(feed.data[0].AGAMA).trigger("change");
                            $('#PENDIDIKAN').val(feed.data[0].PENDIDIKAN).trigger("change");
                            $('#PEKERJAAN').val(feed.data[0].PEKERJAAN).trigger("change");
                            $('#PROVINSI').val(feed.data[0].PROV);
                            $("#PROVINSI").trigger("change");

                            var prov = feed.data[0].PROV;
                            var kab = feed.data[0].KAB;
                            var kec = feed.data[0].KEC;
                            var kel = feed.data[0].DESA;

                            $.post('<?php echo base_url("master/kabupaten"); ?>', {WILAYAH: "KAB", PROV: prov}, function (data) {
                                $('#KAB').html(data);
                                $("#KAB option[value='" + kab + "']").attr("selected", "selected");
                                $("#KAB").select2();

                            });

                            $.post('models/ambil_wilayah.php', {WILAYAH: "KEC", KAB: kab, PROV: prov}, function (data) {
                                $('#KEC').html(data);
                                $("#KEC option[value='" + kec + "']").attr("selected", "selected");
                                $("#KEC").select2();
                            });

                            $.post('models/ambil_wilayah.php', {WILAYAH: "KEL", KEC: kec, KAB: kab, PROV: prov}, function (data) {
                                $('#KEL').html(data);
                                $("#KEL option[value='" + kel + "']").attr("selected", "selected");
                                $("#KEL").select2();
                            });

                        } //data.status

                    });

                }

                //end of function resep pasien


                //ambil data kabupaten

                $('#PROVINSI').change(function () {
                    var kondisi = $("#kondisi_load").val();
                    if (kondisi == 0) {
                        var provinsi = $(this).val();
                        $.getJSON('<?php echo base_url("master/kabupaten"); ?>', {WILAYAH: "KAB", PROV: provinsi}, function (feed) {

                            if (feed.status === 1) {

                                console.log("jumlah item di di data adalah " + feed.data.length);

                                for (i = 0; i < feed.data.length; i++) {
                                    var nilai = feed.data[i].KODE_KAB;
                                    var opsi = feed.data[i].NAMA_KAB;

                                    $("#KAB").append('<option value="' + nilai + '">' + opsi + '</option>');

                                }



                            } else {
                                alert("tidak bisa mengambil data provinsi");
                            }


                            //$('#KAB').html(data);
                        });
                    }
                });



                //ambil data kecamatan
                $('#KAB').change(function () {
                    var kondisi = $("#kondisi_load").val();
                    if (kondisi == 0) {
                        var provinsi = $("#PROVINSI option:selected").val();
                        var kabupaten = $(this).val();
                        $.getJSON('<?php echo base_url("master/kecamatan"); ?>', {WILAYAH: "KEC", KAB: kabupaten, PROV: provinsi}, function (feed) {

                            if (feed.status === 1) {

                                console.log("jumlah item di di data adalah " + feed.data.length);

                                for (i = 0; i < feed.data.length; i++) {
                                    var nilai = feed.data[i].KODE_KEC;
                                    var opsi = feed.data[i].NAMA_KEC;

                                    $("#KEC").append('<option value="' + nilai + '">' + opsi + '</option>');

                                }



                            } else {
                                alert("tidak bisa mengambil data provinsi");
                            }




                            //$('#KEC').html(data);
                        });
                    }
                });

                //ambil data kelurahan/desa
                $('#KEC').change(function () {
                    var kondisi = $("#kondisi_load").val();
                    if (kondisi == 0) {
                        var provinsi = $("#PROVINSI option:selected").val();
                        var kabupaten = $("#KAB option:selected").val();
                        var kecamatan = $(this).val();
                        $.getJSON('<?php echo base_url("master/kelurahan"); ?>', {WILAYAH: "KEL", KEC: kecamatan, KAB: kabupaten, PROV: provinsi}, function (feed) {
                            if (feed.status === 1) {

                                console.log("jumlah item di di data adalah " + feed.data.length);

                                for (i = 0; i < feed.data.length; i++) {
                                    var nilai = feed.data[i].KODE_KEL;
                                    var opsi = feed.data[i].NAMA_KEL;

                                    $("#KEL").append('<option value="' + nilai + '">' + opsi + '</option>');

                                }



                            } else {
                                alert("tidak bisa mengambil data provinsi");
                            }
                        });
                    }
                });


                //dialog lihat data pasien
                dialog = $("#dialog-form").dialog({
                    autoOpen: false,
                    height: 400,
                    width: 600,
                    modal: true

                });

                //saat tombol cari pasien diklik
                $("#TOMBOLCARI").on("click", function () {
                    dialog.dialog("open");
                    $("#searchpasien").click(function (test) {


                        var _nama = $("#cari_nama").val();
                        ;
                        console.log("jumlah huruf " + _nama.length);
                        if (_nama.length > 3) {

                            ambil_data(_nama);
                        } else {
                            alert("masukkan nama minimal 3 huruf ");
                            $("#cari_nama").focus();
                        }




                    });
                });


                function ambil_data(nama) {

                    $.post("models/cari_pasien.php", {nama: nama}, function (data) {
                        if (data != '') {
                            $("#list_pasien").empty();
                            $("#list_pasien").html(data);
                        }
                    });

                }



                $("#TGLLAHIR").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "1950:+nn",
                    dateFormat: "dd-mm-yy"
                });

                $("#TGLKUNJUNGAN").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "2013:+nn",
                    dateFormat: "dd-mm-yy"
                });

                $("select").select2();


            });

        </script>


    </body>
</html>
