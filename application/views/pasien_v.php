<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AdminLTE 2 | Starter</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="<?php echo base_url("assets/bootstrap/css/bootstrap.min.css") ?>">
        <!-- jQueryUI -->
        <link rel="stylesheet" href="<?php echo base_url("assets/plugins/jQueryUI/jquery-ui.min.css") ?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url("assets/plugins/fontawesome/css/font-awesome.min.css") ?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url("assets/plugins/select2/select2.min.css") ?>">
        <!-- dataTables -->
        <link rel="stylesheet" href="<?php echo base_url("assets/plugins/datatables/jquery.dataTables.min.css") ?>">
        <link rel="stylesheet" href="<?php echo base_url("assets/plugins/datatables/dataTables.bootstrap.min.css") ?>">

        <!-- bootstrap toggle -->
        <link rel="stylesheet" href="<?php echo base_url("assets/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css") ?>">
        <!-- Ionicons -->
        <!--link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"--!>
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url("assets/dist/css/AdminLTE.min.css") ?>">
        <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
              page. However, you can choose any other skin. Make sure you
              apply the skin class to the body tag so the changes take effect.
        -->
        <link rel="stylesheet" href="<?php echo base_url("assets/dist/css/skins/skin-blue.min.css") ?>">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <!--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the
    desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
    -->
    <body class="hold-transition skin-blue layout-top-nav">
        <div class="wrapper">
            <?php $this->load->view('template/header') ?>

            <div class="content-wrapper">


                <!-- Content Wrapper. Contains page content -->
                <section class="content">

                    <form role="form" action="models/reg.php" method="POST" class="form-horizontal">
                        <input type="hidden" value="0" name="cari" id="cari">

                        <div class="box box-primary box-solid">

                            <div class="box-header with-border">
                                <h5 class="box-title">
                                    <i class="fa fa-fw fa-table"></i>
                                    Filter Database pasien
                                </h5>

                            </div>

                            <div class="box-body">


                                <div class="row">
                                    <div class="col-sm-2">
                                        <label class="control-label">
                                            No MR
                                        </label>
                                        <input class="form-control" id="nomr" type="text" name="nomr" placeholder="Masukkan No.MR Pasien">
                                    </div>

                                    <div class="col-sm-7">
                                        <label class="control-label">
                                            Nama
                                        </label>
                                        <input class="form-control" id="nama" type="text" name="nama" placeholder="Masukkan Nama Pasien">
                                    </div>





                                    <div class="col-sm-3">
                                        <label class="control-label">ALAMAT </label>
                                        <input type="text"  name="ALAMAT" id="ALAMAT" class="form-control" placeholder="Masukkan Alamat Pasien">
                                    </div>




                                </div>

                            </div>
                            <div class="box-footer">
                                <button type="button" id="cari" class="btn btn-success pull-right">
                                    <i class="fa fa-search">
                                    </i> Pencarian
                                </button>
                            </div>

                        </div>






                        <div class="box box-solid box-primary" id="widget-box-2">
                            <div class="box-header with-header">
                                <h5 class="box-title">
                                    <i class="ace-icon fa fa-table"></i>
                                    Database Pasien
                                </h5>

                            </div>

                            <div class="box-body">

                                <table class="table table-striped table-hover table-bordered table-condesed" id="table_pasien">
                                    <thead>
                                        <tr>
                                            <th>No. MR</th>
                                            <th>Nama Pasien</th>
                                            <th>JK</th>
                                            <th>Tanggal Lahir</th>
                                            <th>Alamat</th>
                                            <th>Proses</th>
                                        </tr>
                                    </thead>



                                    <tfoot>
                                        <tr>
                                            <th>No. MR</th>
                                            <th>Nama Pasien</th>
                                            <th>JK</th>
                                            <th>Tanggal Lahir</th>
                                            <th>Alamat</th>
                                            <th>Prosest</th>
                                        </tr>
                                    </tfoot>
                                </table>



                            </div>
                        </div>
                    </form>







                </section>





            </div><!-- /.content-wrapper -->
            <?php $this->load->view('template/footer') ?>
            <?php $this->load->view('template/control') ?>
        </div> <!-- /.wrapper -->

        <!-- jQuery 2.1.4 -->
        <script src="<?php echo base_url("assets/plugins/jQuery/jQuery-2.1.4.min.js"); ?>"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="<?php echo base_url("assets/bootstrap/js/bootstrap.min.js"); ?>"></script>
        <!-- SlimScroll -->
        <script src="<?php echo base_url("assets/plugins/slimScroll/jquery.slimscroll.min.js"); ?>"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url("assets/plugins/fastclick/fastclick.min.js"); ?>"></script>
        <!-- select2 -->
        <script src="<?php echo base_url("assets/plugins/select2/select2.min.js"); ?>"></script>
        <!-- form-validator -->
        <script src="<?php echo base_url("assets/plugins/form-validator/jquery.form-validator.min.js"); ?>"></script>
        <!-- jqueryUI -->
        <script src="<?php echo base_url("assets/plugins/jQueryUI/jquery-ui.min.js"); ?>"></script>
        <!-- maskinput -->
        <script src="<?php echo base_url("assets/plugins/maskinput/jquery.mask.min.js"); ?>"></script>
        <!-- dataTable -->
        <script src="<?php echo base_url("assets/plugins/datatables/jquery.dataTables.min.js"); ?>"></script>
        <!-- hotkeys -->
        <script src="<?php echo base_url("assets/plugins/hotkeys/jquery.hotkeys.js"); ?>"></script>


        <!-- AdminLTE App -->
        <script src="<?php echo base_url("assets/dist/js/app.min.js"); ?>"></script>
        <script type="text/javascript">

            $(document).ready(function () {


                function pesan(judul, title) {
                    $.alert({
                        title: judul,
                        content: title,
                    });
                }

                $("#table_pasien").on("click", ".hapus", function () {
                    mr = $(this).attr("data-hapus");
                    hapus_data(mr);

                });

                $('#table_pasien').DataTable({
                    "bProcessing": true,
                    "serverSide": true,
                    
                    "ajax": {
                        url: "<?php echo base_url("pasien/listPasien"); ?>", // json datasource
                        type: "post", // type of method  ,GET/POST/DELETE
                        error: function (err) {
                            console.log("data " + err);
                            //$("#employee_grid_processing").css("display","none");
                        },
                        dataFilter: function (reps) {
                            console.log("reps " + reps);
                            return reps;
                        },
                        data: function(d){
                            var nama = $("#nama").val();
                            var alamat = $("#alamat").val();
                            var nomr = $("#nomr").val();
                            var cari = $("#cari").val();
                            var keluarga = $("#keluarga").val();
                            
                            d.cari = cari;
                            d.alamat = alamat;
                            d.nama = nama;
                            d.keluarga = keluarga;
                            d.nomr = nomr;
                        }
                    }
                });

                function hapus_data(mr) {



                    $.confirm({
                        title: 'Konfirmasi Hapus',
                        content: 'Apakah Anda Ingin Menghapus data Pasien dengan No. Pasien <strong>' + mr + ' ?</strong>',
                        buttons: {
                            hapus: {
                                text: 'Ya...',
                                btnClass: 'btn-red',
                                action: function () {
                                    //fungsi ini akan dijalankan saat tombol 'Ya...' di tekan :D
                                    $.post("models/hapus_pasien.php", {mr: mr}, function (data) {
                                        if (data == '1') {

                                            pesan("Berhasil Menghapus data pasien dengan nomr " + mr, "Berhasil hapus");
                                            window.location.reload();
                                        } else if (data == '2') {
                                            pesan("Tidak Bisa Menghapus Pasien ini, karena sudah ada data kunjungan pasien " + mr, "Tidak Bisa menghapus");

                                        } else {
                                            pesan("Tidak Berhasil Menghapus data pasien " + mr, "error hapus");
                                        }

                                    });

                                }
                            },
                            cancel: function () {
                                text: 'batal'
                            }

                        }
                    });




                }

            });






        </script>



    </body>
</html>
