<?php

class Kunjungan_model Extends CI_Model {
    
    
    
    
    
    
    
    public function load($nomr) {
        
        $query = $this->db->select('*')->where('MR', $nomr)->get('MASTER_PASIEN')->result();
        
        return $query;
       
    }
    
    public function listPasien($data) {
        
        
        if($data['cari']) {
            
           
            if(isset($data['nama'])){
               $sNama = " and NAMA LIKE ".$data['nama'] ."%";
            } else {
                $sNama = "";
            } 
            
            if(isset($data['nomr'])){
               $sNomr = " and NOMR = ".$data['nomr'] ."%";
            } else {
                $sNomr = "";
            } 
            
            if(isset($data['alamat'])){
               $sAlamat = " and ALAMAT = ".$data['alamat'] ."%";
            } else {
                $sAlamat = "";
            } 
            
            if(isset($data['KELUARGA'])){
               $sKeluarga = " and KELUARGA = ".$data['keluarga'] ."%";
            } else {
                $sKeluarga = "";
            } 
            
            $query_pasien = "select MR, NAMA, GENDER, TGL_LAHIR, ALAMAT where NOMR is not NULL ";
            
            $queryPasien = $this->db->query($query_pasien.$sNomr.$sNama.$sAlamat.$sKeluarga);
        } else {
            
            $queryPasien = $this->db->select("MR, NAMA, GENDER, TGL_LAHIR, ALAMAT")->get('master_pasien');
                        
        }
        
        
        $dataPasien = $queryPasien->result();
        $jumlah_row = $queryPasien->num_rows();
        
        
        foreach ($dataPasien as $row)
        {
            
            $tombol_edit = '<button type="button" class="btn btn-sm btn-warning pasien" id="nomr'.$row->MR.'"><i class="fa fa-edit"></i></button>&nbsp;';
            $tombol_hapus = '<button type="button" class="btn btn-sm btn-danger hapus" id="nomr'.$row->MR.'"><i class="fa fa-trash"></i></button>';
            
          
            
            
            
            $arrPasien[] = array(
              $row->MR,
              $row->NAMA,
              $row->GENDER,
              $row->TGL_LAHIR,
              $row->ALAMAT,
              $tombol_edit.$tombol_hapus
                    
            ); 
        }
        
        	$json_data = array(
			"draw"            => intval( $data['draw'] ),
			"recordsTotal"    => intval( $jumlah_row ),
			"recordsFiltered" => intval($jumlah_row),
			"data"            => $arrPasien   // total data array
			);

                        //var_dump($json_data);
        
	return json_encode($json_data);
        
        
    }
    
    
    
}

?>
