<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Master_model
 *
 * @author drakenzull
 */
class Master_model Extends CI_Model {
    //put your code here
    
    
    public function masterData($tipe) {
        
        $nama_table = "master_".$tipe;
        $nama_kolom = "nama_".$tipe;
        
        $this->db->from($nama_table);
        $this->db->order_by($nama_kolom,'asc');
        return $query = $this->db->get();
        
        
    }
    
    
    public function pendidikan() {
       
           return $query = $this->db->get('master_pendidikan');
    }
    
    
    public function provinsi() {
        
       $query = $this->db->get('master_provinsi');
       if($query) {
           return $query;
       }
       
    }
    
    public function kabupaten($provinsi) {
        
        
        $this->db->where('KODE_PROV', $provinsi);
        $this->db->from('master_kabupaten');
        $query = $this->db->get()->result();
     
       if($query) {
           return $query;
       }
       
    }
    
    public function kecamatan($provinsi, $kabupaten) {
        
        $data_kec = array('KODE_PROV'=>$provinsi, 'KODE_KAB'=>$kabupaten);
        $this->db->where($data_kec);
        $this->db->from('master_kecamatan');
        $query = $this->db->get()->result();
     
       if($query) {
           return $query;
       }
       
    }

    public function kelurahan($provinsi, $kabupaten, $kecamatan) {
        
        $data_kel = array('KODE_PROV'=>$provinsi, 'KODE_KAB'=>$kabupaten, 'KODE_KEC'=>$kecamatan);
        $this->db->where($data_kel);
        $this->db->from('master_kelurahan');
        $query = $this->db->get()->result();
     
       if($query) {
           return $query;
       }
       
    }
       
}
