<?php

if(!defined('BASEPATH'))
    exit('Akses Script secara lansung tidak dibolehkan');

class MY_Controller extends CI_Controller 
{
    public function __contruct()
    {
        parent::__contruct();
        
        session_start();
        
        //cek session login
        if($this->session->userdata('login') == FALSE)
        {
            redirect('login');
        }
    }
}
