<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pendaftaran
 *
 * @author drakenzull
 */
class Kunjungan extends CI_Controller {
    //put your code here
    
    public function __construct() {
        parent::__construct();
        
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Kunjungan_model', 'kunjungan', TRUE);
        
        
    }
    
    public function index() {
        $this->load->view("kunjungan_v");
    }
    
    public function load() {
        
        $nomr = $this->input->post("nomr");
        $data = $this->pasien->load($nomr);
        
        
        if($data) {
            $return = array("status"=>TRUE, "data"=>$data, "pesan"=>"Berhasil Mengambil data pasien");
           
        } else {
           $return = array("status"=>FALSE, "pesan"=>"Tidak Berhasil Mengambil data pasien");
        }
        
        header('Content-Type: application/json');
         echo json_encode($return);
        
    }

        public function listPasien() {
        
      
        $data = $this->pasien->listPasien($this->input->post());      
        
        header('Content-Type: application/json');
        
        echo $data;
        
    }
    
    
    
    
    
}
