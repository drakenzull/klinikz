<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pendaftaran
 *
 * @author drakenzull
 */
class Pendaftaran extends CI_Controller {
    //put your code here
    
    public function __construct() {
        parent::__construct();
        
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('pendaftaran_model', 'pendaftaran', TRUE);
        $this->load->model('master_model', 'master', TRUE);
        
        
    }
    
    public function index() {
        
        
        $data['pendidikan'] = $this->master->masterData('pendidikan')->result();
        //$data['pekerjaan'] = $this->master->pekerjaan()->result();
        $data['pekerjaan'] = $this->master->masterData('pekerjaan')->result();
        $data['provinsi'] = $this->master->provinsi()->result();
        $data['dokter'] = $this->master->masterData('dokter')->result();
        $data['unit'] = $this->master->masterData('unit')->result();
        $data['kelompok'] = $this->master->masterData('kelompok')->result();
        $this->load->view("pendaftaran_v", $data);
        
        
    }
    
    
    
    
}
