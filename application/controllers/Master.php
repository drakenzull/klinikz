<?php

class Master extends CI_Controller
  {
  
        public function __construct() {
            parent::__construct();
            
            $this->load->model("Master_model", "master", true);
            
        }
    
        
        public function kabupaten() {
            
            $provinsi = $this->input->get("PROV");
            
            $query = $this->master->kabupaten($provinsi);
            
            if($query) {
                
                $feed = array(
                  "status"=>1,
                  "data"=>$query,
                  "pesan"=>"Berhasil Mengambil data kabupaten"
                
                );
                
            } else {
                $feed = array(
                  "status"=>0,
                  "pesan"=>"Tidak berhasil mengambil data kabupaten"
                
                );
            }
            
            header("Content-type: application/json");
            echo json_encode($feed);
            
        }
        
        public function kecamatan() {
            
            $provinsi = $this->input->get("PROV");
            $kabupaten = $this->input->get("KAB");
            
            $query = $this->master->kecamatan($provinsi, $kabupaten);
            
            if($query) {
                
                $feed = array(
                  "status"=>1,
                  "data"=>$query,
                  "pesan"=>"Berhasil Mengambil data kabupaten"
                
                );
                
            } else {
                $feed = array(
                  "status"=>0,
                  "pesan"=>"Tidak berhasil mengambil data kabupaten"
                
                );
            }
            
            header("Content-type: application/json");
            echo json_encode($feed);
            
        }

        public function kelurahan() {
            
            
            $provinsi = $this->input->get("PROV");
            $kabupaten = $this->input->get("KAB");
            $kecamatan = $this->input->get("KEC");
            
            $query = $this->master->kelurahan($provinsi, $kabupaten, $kecamatan);
            
            if($query) {
                
                $feed = array(
                  "status"=>1,
                  "data"=>$query,
                  "pesan"=>"Berhasil Mengambil data kabupaten"
                
                );
                
            } else {
                $feed = array(
                  "status"=>0,
                  "pesan"=>"Tidak berhasil mengambil data kabupaten"
                
                );
            }
            
            header("Content-type: application/json");
            echo json_encode($feed);
            
        }
        
    
}
