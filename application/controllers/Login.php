<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
                $this->load->helper('form');
                $this->load->library('form_validation');
		$this->load->model('login_model', 'login', TRUE);
	}

	public function index() {
            
            if($this->session->userdata('login') == TRUE) {
               redirect("Dashboard");
            } else {
                if($this->login->validasi()) {
                    if($this->login->cek_user())
                    {
                        redirect('Dashboard');
                    } else {
                        $this->data['pesan'] = "Username dan password salah";
                        $this->load->view('login/login_form', $this->data);
                        
                    }
                    
                } else {
                     $this->data['pesan'] = "Anda Harus Login Terlebih Dahulu";
                    $this->load->view('login/login_form', $this->data);
                } 
            }
		
	}
        
        public function logout()
        {
            $this->login->logout();
            $this->load->view('login/login_form');
        }
}